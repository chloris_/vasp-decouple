# vasp-decouple

Decouples a structure in VASP POSCAR format into two POSCAR structures with
use of a filter structure. The first structure contains only atoms also found
in the filter structure, while the second contains the leftover atoms, that
were not found in the filter.


## Building

`vasp-decouple` is written in [Rust](https://www.rust-lang.org/) and can be
easily built with [`cargo`](https://rustup.rs/):

```sh
cargo build --release
```


## Usage

Run the program with `-h` or `--help` flags to get usage information.
As an example, this command will decouple file `CONTCAR`, using
`POSCAR_FILTER` as a filter file. The structure will be decoupled using
tolerance of 0.4&nbsp;Å and the resulting files (filtered structure and
leftover structure) will be placed in directory `decoupled`.

```sh
vasp-decouple -i CONTCAR -f POSCAR_FILTER -t 0.4 -o decoupled
```


## Notes

### Filtering and tolerance

If tolerance is not provided, an atom in the input structure will only match
an atom in the filter structure, when their symbols match and their positions
match exactly. If tolerance is provided, distance between scaled Cartesian
atom positions is calculated. The atoms are considered matched if the distance
is shorter than the tolerance (in addition to matching symbols).

This behaviour is useful for filtering geometrically optimized structures,
where the atoms in the input structure have moved relatively to the filter
structure. Tolerance allows the atoms to still be recognized and filtered out.
However, false positives are possible and the group of atoms, that move the
least during the optimization, should be used as a filter, if possible.

### Sorting

The program can sort the resulting filtered and leftover atom lists to always
produce POSCAR files with the same order of atom groups, allowing
order-dependent POTCAR files to be reused. To activate sorting, the
appropriate command line flag needs to be passed to the program
(see program usage).
