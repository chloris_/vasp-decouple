//! POSCAR decoupling functions.

use crate::atom_list::AtomList;
use vasp_poscar::Poscar;

#[cfg(test)]
use crate::atom_list::Atom;

/// Decouples `combined` and `filter` POSCAR files into two POSCAR files, returned
/// as a tuple. The first one contains the atoms that are also found in the `filter`
/// file, and the second one contains all the remaining atoms. Sorts the resulting
/// atoms by atom symbol if `sort` is [true].
///
/// In case of errors, a string with an error message is returned.
pub fn decouple_poscar(
    combined: &Poscar,
    filter: &Poscar,
    sort: bool,
) -> Result<(Poscar, Poscar), String> {
    use crate::atom_list::atom_list_to_poscar;
    use crate::atom_list::poscar_to_atom_list;

    // Checks if cells are the same
    if !are_cells_identical(combined, filter) {
        return Err(String::from(
            "The cells from POSCAR files are not of \
            the same dimensions.",
        ));
    }

    // Extract atom lists from POSCAR files
    let filter_atom_list = poscar_to_atom_list(filter)?;
    let combined_atom_list = poscar_to_atom_list(combined)?;

    // Filter the lists and turn the data back into POSCAR format
    let (filtered_list, leftover_list) = decouple_atom_list(&combined_atom_list, &filter_atom_list);

    let template_poscar = combined.clone().into_raw();
    let filtered_poscar = match atom_list_to_poscar(&filtered_list, &template_poscar, sort) {
        Ok(poscar) => poscar,
        Err(e) => return Err(format!("Filtered POSCAR creation error: {}", e)),
    };
    let leftover_poscar = match atom_list_to_poscar(&leftover_list, &template_poscar, sort) {
        Ok(poscar) => poscar,
        Err(e) => return Err(format!("Leftover POSCAR creation error: {}", e)),
    };

    return Ok((filtered_poscar, leftover_poscar));
}

/// Performs similar action as [decouple_poscar], except that it allows the atoms
/// to move. Consequently, it can decouple a result of geometric optimization.
/// The atoms in `filter` POSCAR match, if the distance between matched
/// atoms is not greater than `max_distance`, and the unit cells need not be
/// identical.
pub fn decouple_poscar_tolerant(
    combined: &Poscar,
    filter: &Poscar,
    max_distance: f64,
    sort: bool,
) -> Result<(Poscar, Poscar), String> {
    use crate::atom_list::atom_list_to_poscar;
    use crate::atom_list::poscar_to_atom_list;

    // Validate parameter
    if max_distance < 0.0 {
        return Err("Maximum distance must not be negative.".to_string());
    }

    // Extract atom lists from POSCAR files
    let filter_atom_list = poscar_to_atom_list(filter)?;
    let combined_atom_list = poscar_to_atom_list(combined)?;

    // Filter the lists and turn the data back into POSCAR format
    let (filtered_list, leftover_list) =
        decouple_atom_list_tolerant(&combined_atom_list, &filter_atom_list, max_distance);

    let template_poscar = combined.clone().into_raw();
    let filtered_poscar = match atom_list_to_poscar(&filtered_list, &template_poscar, sort) {
        Ok(poscar) => poscar,
        Err(e) => return Err(format!("Filtered POSCAR creation error: {}", e)),
    };
    let leftover_poscar = match atom_list_to_poscar(&leftover_list, &template_poscar, sort) {
        Ok(poscar) => poscar,
        Err(e) => return Err(format!("Leftover POSCAR creation error: {}", e)),
    };

    return Ok((filtered_poscar, leftover_poscar));
}

/// Compares scaled lattice vectors of both POSCAR files and returns `true` if
/// they are identical or `false` otherwise.
///
/// The check is very simple and may produce false-negatives.
fn are_cells_identical(combined: &Poscar, filter: &Poscar) -> bool {
    let combined_vectors = combined.scaled_lattice_vectors();
    let filter_vectors = filter.scaled_lattice_vectors();

    return combined_vectors == filter_vectors;
}

/// Decouples `combined` atom list into two lists using `filter` atom list, and
/// returns them in a tuple.
///
/// The first list contains atoms, that are found in both the combined and filter
/// lists. The second returned atom list contains any leftover atoms.
fn decouple_atom_list(combined: &AtomList, filter: &AtomList) -> (AtomList, AtomList) {
    let mut filtered_list = AtomList::new();
    let mut leftover_list = AtomList::new();

    for atom in combined.iter() {
        // Debug print
        #[cfg(test)]
        println!("Atom \"{}\" {:?} ...", atom.symbol, atom.position);

        match filter
            .iter()
            .find(|&x| x.symbol == atom.symbol && x.position == atom.position)
        {
            Some(_x) => {
                filtered_list.push(atom.clone());

                // Debug print
                #[cfg(test)]
                println!(
                    " [×] found in filter as \"{}\" {:?}",
                    _x.symbol, _x.position
                );
            }
            None => {
                leftover_list.push(atom.clone());

                // Debug print
                #[cfg(test)]
                println!(" [ ] NOT found in filter list");
            }
        }
    }

    return (filtered_list, leftover_list);
}

/// Similar to [decouple_atom_list], except that two atoms are considered matched,
/// if they are up to `max_distance` apart. [crate::atom_list::Atom]s are compared
/// by their scaled Cartesian coordinates instead of fractional coordinates.
fn decouple_atom_list_tolerant(
    combined: &AtomList,
    filter: &AtomList,
    max_distance: f64,
) -> (AtomList, AtomList) {
    let mut filtered_list = AtomList::new();
    let mut leftover_list = AtomList::new();

    for atom in combined.iter() {
        match filter.iter().find(|&filter_atom| {
            use crate::poscar_utils::calculate_distance;

            const CART_ERROR: &'static str = "Cartesian coordinates are mandatory \
                for tolerant atom list decoupling.";

            let filter_cart = filter_atom.cart_position.expect(CART_ERROR);
            let atom_cart = atom.cart_position.expect(CART_ERROR);

            let distance = calculate_distance(atom_cart, filter_cart);

            atom.symbol == filter_atom.symbol && distance <= max_distance
        }) {
            Some(_x) => {
                filtered_list.push(atom.clone());

                // Debug print
                #[cfg(test)]
                println!(
                    "[×] Atom \"{}\" {:?} matched to filter \"{}\" {:?}",
                    atom.symbol, atom.position, _x.symbol, _x.position
                );
            }
            None => {
                leftover_list.push(atom.clone());

                // Debug print
                #[cfg(test)]
                println!(
                    "[ ] Atom \"{}\" {:?} NOT matched to filter list",
                    atom.symbol, atom.position
                );
            }
        }
    }

    return (filtered_list, leftover_list);
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::test_poscars;

    /// Asserts if two POSCARS represent the same structure. Panics in case of
    /// discrepancies.
    fn assert_poscars(p1: &Poscar, p2: &Poscar) {
        assert_poscars_tolerant(p1, p2, 0.0);

        // Old non-tolerant check code
        /*
        use crate::atom_list::poscar_to_atom_list;

        // Check lattice
        if !are_cells_identical(p1, p2) {
            panic!("Unit cells are not identical:\nVectors 1: {:?}\nVectors 2: {:?}.",
                   p1.scaled_lattice_vectors(),
                   p2.scaled_lattice_vectors());
        }

        // Check site numbers
        assert_eq!(p1.num_sites(), p2.num_sites(), "Site counts are not equal.");

        // Extract atom lists from poscar to compare them
        let al1 = poscar_to_atom_list(p1).unwrap();
        let al2 = poscar_to_atom_list(p2).unwrap();
        assert_eq!(al1.len(), al2.len(),
            "Extracted atom lists have different atom counts.");

        // Every atom in `p1` can be found in `p2`
        for atom1 in al1.iter() {
            if al2.iter().find(|&atom2| atom1 == atom2).is_none() {
                panic!("Atom \"{}\" with position {:?} is not found in both POSCARs.",
                       atom1.symbol, atom1.position);
            }
        }

        // Every atom in `p2` can be found in `p1`
        for atom2 in al2.iter() {
            if al1.iter().find(|&atom1| atom2 == atom1).is_none() {
                panic!("Atom \"{}\" with position {:?} is not found in both POSCARs.",
                       atom2.symbol, atom2.position);
            }
        }
        */
    }

    /// Asserts if two POSCARS represent the same structure and panics in case of
    /// discrepancies.
    ///
    /// In order for two structures to be considered equal, the lattices and site
    /// numbers must be identical, in addition that every atom found in `p1` is
    /// found in `p2` and vice-versa.
    ///
    /// Atoms are considered the same if their symbols are equal and their
    /// fractional positions differ for no more than `tolerance` absolutely.
    fn assert_poscars_tolerant(p1: &Poscar, p2: &Poscar, tolerance: f64) {
        use crate::atom_list::poscar_to_atom_list;
        use float_cmp::approx_eq;

        // Check lattice
        if !are_cells_identical(p1, p2) {
            panic!(
                "Unit cells are not identical:\nVectors 1: {:?}\nVectors 2: {:?}.",
                p1.scaled_lattice_vectors(),
                p2.scaled_lattice_vectors()
            );
        }

        // Check site numbers
        assert_eq!(p1.num_sites(), p2.num_sites(), "Site counts are not equal.");

        // Extract atom lists from poscar to compare them
        let al1 = poscar_to_atom_list(p1).unwrap();
        let al2 = poscar_to_atom_list(p2).unwrap();
        assert_eq!(
            al1.len(),
            al2.len(),
            "Extracted atom lists have different atom counts."
        );

        // Closure to check atom identity
        let are_atoms_identical = |atom1: &Atom, atom2: &Atom| -> bool {
            // Check atom symbols
            if atom1.symbol != atom2.symbol {
                return false;
            }

            // Check matching coordinates (within tolerance)
            for (f1, f2) in atom1.position.iter().zip(atom2.position.iter()) {
                if approx_eq!(f64, *f1, *f2, epsilon = tolerance) {
                    return true;
                }
            }

            return false;
        };

        // Every atom in `p1` can be found in `p2`
        for atom1 in al1.iter() {
            if al2
                .iter()
                .find(|&atom2| are_atoms_identical(atom1, atom2))
                .is_none()
            {
                panic!(
                    "Atom \"{}\" with position {:?} is not found in both POSCARs.",
                    atom1.symbol, atom1.position
                );
            }
        }

        // Every atom in `p2` can be found in `p1`
        for atom2 in al2.iter() {
            if al1
                .iter()
                .find(|&atom1| are_atoms_identical(atom2, atom1))
                .is_none()
            {
                panic!(
                    "Atom \"{}\" with position {:?} is not found in both POSCARs.",
                    atom2.symbol, atom2.position
                );
            }
        }
    }

    #[test]
    fn test_are_cells_identical() {
        let combined_poscar =
            Poscar::from_reader(test_poscars::POSCAR_FILLED_FRAMEWORK.as_bytes()).unwrap();
        let filter_poscar = Poscar::from_reader(test_poscars::POSCAR_FRAMEWORK.as_bytes()).unwrap();

        // Test with intact data - the cells should be identical
        let result = are_cells_identical(&combined_poscar, &filter_poscar);
        assert_eq!(result, true);

        // Modify the data - the cells are no longer identical
        let mut modified_poscar = filter_poscar.clone().into_raw();
        // Simply change a single component of a single lattice vector
        modified_poscar.lattice_vectors[1][0] = 3.333;
        let modified_poscar = modified_poscar.validate().unwrap();

        let result = are_cells_identical(&combined_poscar, &modified_poscar);
        assert_eq!(result, false);
    }

    #[test]
    fn test_decouple_atom_list() {
        let test_cases = [
            // Case 1
            (
                // Combined list
                vec![
                    Atom::new("Zn", [1.0, 2.0, 3.0]),
                    Atom::new("Au", [1.5, 2.2, 8.0]),
                    Atom::new("O", [1.0, 2.0, 3.0]),
                    Atom::new("Zn", [0.4, 4.1, 6.2]),
                    Atom::new("N", [0.5, 0.1, 3.0]),
                    Atom::new("N", [0.3, 9.0, 2.0]),
                    Atom::new("N", [9.9, 0.9, 0.3]),
                    Atom::new("Ag", [0.22, 0.97, 0.67]),
                ],
                // Filter list
                vec![
                    Atom::new("Zn", [0.4, 4.1, 6.2]),
                    Atom::new("N", [0.3, 9.0, 2.0]),
                    Atom::new("Au", [1.5, 2.2, 8.0]),
                ],
                // Result: filtered list
                vec![
                    Atom::new("Au", [1.5, 2.2, 8.0]),
                    Atom::new("Zn", [0.4, 4.1, 6.2]),
                    Atom::new("N", [0.3, 9.0, 2.0]),
                ],
                // Result: leftover list
                vec![
                    Atom::new("Zn", [1.0, 2.0, 3.0]),
                    Atom::new("O", [1.0, 2.0, 3.0]),
                    Atom::new("N", [0.5, 0.1, 3.0]),
                    Atom::new("N", [9.9, 0.9, 0.3]),
                    Atom::new("Ag", [0.22, 0.97, 0.67]),
                ],
            ),
        ];

        for (combined, filter, exp_filtered, exp_leftover) in test_cases.iter() {
            let (filtered, leftover) = decouple_atom_list(combined, filter);
            assert_eq!(filtered, *exp_filtered);
            assert_eq!(leftover, *exp_leftover);
        }
    }

    #[test]
    fn test_decouple_poscar() {
        // Load sample imput POSCAR files
        let combined_poscar =
            Poscar::from_reader(test_poscars::POSCAR_BOROHYDRIDE.as_bytes()).unwrap();
        let filter_poscar =
            Poscar::from_reader(test_poscars::POSCAR_BOROHYDRIDE_SAMPLE_FILTER.as_bytes()).unwrap();

        // Perform decoupling
        let (filtered_poscar, leftover_poscar) =
            decouple_poscar(&combined_poscar, &filter_poscar).unwrap();

        // Load expected POSCAR files and compare them to the actual results
        let exp_leftover_poscar =
            Poscar::from_reader(test_poscars::EXPECTED_POSCAR_BOROHYDRIDE_LEFTOVER.as_bytes())
                .unwrap();
        let exp_filtered_poscar =
            Poscar::from_reader(test_poscars::EXPECTED_POSCAR_BOROHYDRIDE_FILTERED.as_bytes())
                .unwrap();

        println!("Checking leftover POSCAR");
        assert_poscars(&leftover_poscar, &exp_leftover_poscar);
        println!("Checking filtered POSCAR");
        assert_poscars(&filtered_poscar, &exp_filtered_poscar);
    }

    #[test]
    fn test_decouple_poscar_tolerant() {
        // Load sample imput CONTCAR and filter POSCAR files
        let contcar =
            Poscar::from_reader(test_poscars::CONTCAR_FILLED_FRAMEWORK.as_bytes()).unwrap();
        let filter_poscar = Poscar::from_reader(test_poscars::POSCAR_FRAMEWORK.as_bytes()).unwrap();

        // Perform decoupling
        let (filtered_poscar, leftover_poscar) =
            decouple_poscar_tolerant(&contcar, &filter_poscar, 0.6).unwrap();

        // Load expected POSCAR files and compare them to the actual results
        let exp_leftover_poscar =
            Poscar::from_reader(test_poscars::EXPECTED_CONTCAR_LEFTOVER.as_bytes()).unwrap();
        let exp_filtered_poscar =
            Poscar::from_reader(test_poscars::EXPECTED_CONTCAR_FILTERED_FRAMEWORK.as_bytes())
                .unwrap();

        println!("Checking filtered POSCAR");
        assert_poscars(&filtered_poscar, &exp_filtered_poscar);
        println!("Checking leftover POSCAR");
        assert_poscars(&leftover_poscar, &exp_leftover_poscar);
    }
}
