//! Structure representing a list of atoms with fractional coordinates instead
//! of separate lists of sites, group numbers and atom symbols.

use std::collections::HashMap;
use vasp_poscar::{Builder, Coords, Poscar, RawPoscar, ValidationError};

/// A container for `Atom` instances.
pub type AtomList = Vec<Atom>;

/// A representation of an atom with information about the atom's chemical
/// type and its position in the unit cell.
#[derive(PartialEq, Debug, Clone)]
pub struct Atom {
    /// Chemical symbol of the atom
    pub symbol: String,
    /// Position of the atom in fractional coordinates
    pub position: [f64; 3],
    /// Position of the atom in scaled Cartesian coordinates. Useful when
    /// calculation of interatomic distances is required.
    pub cart_position: Option<[f64; 3]>,
}

impl Atom {
    /// Creates and returns a new instance of `Self` from provided chemical
    /// `symbol` and the atom's `position` in the unit cell.
    pub fn new(symbol: &str, position: [f64; 3]) -> Self {
        Self {
            symbol: String::from(symbol),
            position: position,
            cart_position: None,
        }
    }
}

/// Parses information in the provided POSCAR file and returns a list of atoms
/// in the unit cell.
///
/// In case of errors an error string is returned.
pub fn poscar_to_atom_list(poscar: &Poscar) -> Result<AtomList, String> {
    use crate::poscar_utils::get_group_symbols_from_comment;

    //
    // Data acquisition
    //

    let symbols: Vec<String> = {
        // Get atom symbols directly from POSCAR file, if provided
        if let Some(group_symbols) = poscar.group_symbols() {
            group_symbols.map(|symbol| String::from(symbol)).collect()
        // Otherwise try to get the symbols from the comment
        } else {
            get_group_symbols_from_comment(poscar.comment())
        }
    };
    if symbols.is_empty() {
        return Err(String::from(
            "Could not get atom symbol list from the POSCAR comment.",
        ));
    }

    // Get atom group counts
    let group_counts = poscar.group_counts().collect::<Vec<usize>>();
    if group_counts.is_empty() {
        return Err(String::from("Could not get atom group counts."));
    }

    // Get atom positions
    let positions = poscar.frac_positions();
    if positions.is_empty() {
        return Err(String::from("Could not get atom positions."));
    }

    let cart_positions = poscar.scaled_cart_positions();

    //
    // Data validation
    //

    // Numbers of atom symbols and groups must match
    if symbols.len() != group_counts.len() {
        return Err(format!(
            "Number of atom symbols ({}) does not match the \
            number of group counts ({}).",
            symbols.len(),
            group_counts.len()
        ));
    }

    // Number of atom positions must match the total of group counts
    let group_count_total = group_counts.iter().sum();
    if positions.len() != group_count_total {
        return Err(format!(
            "Number of atom positions ({}) does not match the \
            total of group counts ({}).",
            positions.len(),
            group_count_total
        ));
    }

    //
    // Data parse
    //

    let mut atom_list = AtomList::new();

    // An iterator over group counts and atom symbols to get the next pair of data
    // once the current one "runs out"
    let mut group_symbol_iter = group_counts.iter().zip(symbols.iter());
    // Remaining atoms that can be processed before next group must be requested
    let mut remaining_atoms: usize = 0;
    // Atom symbol of the group that is currently being parsed
    let mut current_symbol: &str = "";

    for (i, position) in positions.iter().enumerate() {
        // Move to the next group, if necessary
        if remaining_atoms == 0 {
            match group_symbol_iter.next() {
                Some((group_count, symbol)) => {
                    remaining_atoms = *group_count;
                    current_symbol = symbol;
                }
                None => {
                    return Err(format!(
                        "Ran out of groups while processing atom \
                        number {}.",
                        i + 1
                    ));
                }
            }
        }

        // Add the site to the atom list
        let mut atom = Atom::new(current_symbol, *position);
        if let Some(cart_position) = cart_positions.get(i) {
            atom.cart_position = Some(*cart_position);
        }

        atom_list.push(atom);
        remaining_atoms -= 1;
    }

    return Ok(atom_list);
}

/// Attempts to convert `atom_list` back to POSCAR format. Atom sites and symbols
/// are populated with data from the atom list. Lattice data is copied from the
/// `template` POSCAR file.
///
/// If `sort` is [true], POSCAR is produced with atom groups sorted by atom
/// symbol.
pub fn atom_list_to_poscar(
    atom_list: &AtomList,
    template: &RawPoscar,
    sort: bool,
) -> Result<Poscar, ValidationError> {
    // Count atom symbol occurences and produce a sorted copy of atom positions
    let mut symbol_counts = HashMap::<String, usize>::new();
    let mut sorted_positions = HashMap::<String, Vec<[f64; 3]>>::new();

    for atom in atom_list.iter() {
        let new_count: usize;
        match symbol_counts.get(&atom.symbol) {
            Some(count) => new_count = count + 1,
            None => new_count = 1,
        };
        symbol_counts.insert(atom.symbol.clone(), new_count);

        match sorted_positions.get_mut(&atom.symbol) {
            Some(vec) => vec.push(atom.position),
            None => {
                sorted_positions.insert(atom.symbol.clone(), Vec::new());
                sorted_positions
                    .get_mut(&atom.symbol)
                    .unwrap()
                    .push(atom.position);
            }
        };
    }

    // Prepare keys (atom symbols) to iterate through and sort them if required
    // (mapping the keys is a waste and only required because of optional sorting)
    let mut keys: Vec<&str> = symbol_counts.keys().map(|k| k.as_str()).collect();
    if sort {
        // Sort the atom symbols to always obtain results with the same group orders
        keys.sort();
    }

    // Construct group symbols, group counts, and positions for building POSCAR
    let mut group_symbols = Vec::<String>::new();
    let mut group_counts = Vec::<usize>::new();
    let mut positions = Vec::<[f64; 3]>::new();

    for symbol in keys {
        let count = symbol_counts.get(symbol).unwrap();

        group_symbols.push(symbol.to_string());
        group_counts.push(*count);

        let symbol_positions = sorted_positions.get(symbol).unwrap();
        for pos in symbol_positions.iter() {
            positions.push(*pos);
        }
    }

    // Build the POSCAR
    Builder::new()
        .scale(template.scale)
        .lattice_vectors(&template.lattice_vectors)
        .group_symbols(group_symbols)
        .group_counts(group_counts)
        .positions(Coords::Frac(positions))
        .build()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::test_poscars;

    #[test]
    fn test_poscar_to_atom_list() {
        let expected_atom_list = vec![
            Atom::new(
                "H",
                [0.900000000000000, 0.250000000000000, 0.956000000000000],
            ),
            Atom::new(
                "H",
                [0.600000000000000, 0.750000000000000, 0.456000000000000],
            ),
            Atom::new(
                "H",
                [0.100000000000000, 0.750000000000000, 0.044000000000000],
            ),
            Atom::new(
                "H",
                [0.400000000000000, 0.250000000000000, 0.544000000000000],
            ),
            Atom::new(
                "H",
                [0.404000000000000, 0.250000000000000, 0.280000000000000],
            ),
            Atom::new(
                "H",
                [0.096000000000000, 0.750000000000000, 0.780000000000000],
            ),
            Atom::new(
                "H",
                [0.596000000000000, 0.750000000000000, 0.720000000000000],
            ),
            Atom::new(
                "H",
                [0.904000000000000, 0.250000000000000, 0.220000000000000],
            ),
            Atom::new(
                "H",
                [0.172000000000000, 0.054000000000000, 0.428000000000000],
            ),
            Atom::new(
                "H",
                [0.328000000000000, 0.554000000000000, 0.928000000000000],
            ),
            Atom::new(
                "H",
                [0.328000000000000, 0.946000000000000, 0.928000000000000],
            ),
            Atom::new(
                "H",
                [0.828000000000000, 0.554000000000000, 0.572000000000000],
            ),
            Atom::new(
                "H",
                [0.828000000000000, 0.946000000000000, 0.572000000000000],
            ),
            Atom::new(
                "H",
                [0.672000000000000, 0.446000000000000, 0.072000000000000],
            ),
            Atom::new(
                "H",
                [0.672000000000000, 0.054000000000000, 0.072000000000000],
            ),
            Atom::new(
                "H",
                [0.172000000000000, 0.446000000000000, 0.428000000000000],
            ),
            Atom::new(
                "B",
                [0.304000000000000, 0.250000000000000, 0.430500000000000],
            ),
            Atom::new(
                "B",
                [0.196000000000000, 0.750000000000000, 0.930500000000000],
            ),
            Atom::new(
                "B",
                [0.696000000000000, 0.750000000000000, 0.569500000000000],
            ),
            Atom::new(
                "B",
                [0.804000000000000, 0.250000000000000, 0.069500000000000],
            ),
            Atom::new(
                "Li",
                [0.156800000000000, 0.250000000000000, 0.101500000000000],
            ),
            Atom::new(
                "Li",
                [0.343200000000000, 0.750000000000000, 0.601500000000000],
            ),
            Atom::new(
                "Li",
                [0.843200000000000, 0.750000000000000, 0.898500000000000],
            ),
            Atom::new(
                "Li",
                [0.656800000000000, 0.250000000000000, 0.398500000000000],
            ),
        ];

        let poscar = Poscar::from_reader(test_poscars::POSCAR_BOROHYDRIDE.as_bytes()).unwrap();
        let result = poscar_to_atom_list(&poscar).unwrap();

        // Test parsed number of atoms
        assert_eq!(result.len(), expected_atom_list.len());

        // Test the type and position of each parsed atom. The test is very simple
        // and requires that the expected atoms are in exactly the same order as
        // parsed atoms.
        for (res_atom, exp_atom) in result.iter().zip(expected_atom_list.iter()) {
            assert_eq!(res_atom.symbol, exp_atom.symbol);
            assert_eq!(res_atom.position, exp_atom.position);
        }
    }

    #[test]
    fn test_atom_list_to_poscar() {
        let mixed_atom_list = vec![
            Atom::new(
                "Li",
                [0.843200000000000, 0.750000000000000, 0.898500000000000],
            ),
            Atom::new(
                "H",
                [0.100000000000000, 0.750000000000000, 0.044000000000000],
            ),
            Atom::new(
                "H",
                [0.400000000000000, 0.250000000000000, 0.544000000000000],
            ),
            Atom::new(
                "H",
                [0.096000000000000, 0.750000000000000, 0.780000000000000],
            ),
            Atom::new(
                "H",
                [0.900000000000000, 0.250000000000000, 0.956000000000000],
            ),
            Atom::new(
                "H",
                [0.596000000000000, 0.750000000000000, 0.720000000000000],
            ),
            Atom::new(
                "H",
                [0.904000000000000, 0.250000000000000, 0.220000000000000],
            ),
            Atom::new(
                "H",
                [0.172000000000000, 0.054000000000000, 0.428000000000000],
            ),
            Atom::new(
                "H",
                [0.328000000000000, 0.554000000000000, 0.928000000000000],
            ),
            Atom::new(
                "H",
                [0.328000000000000, 0.946000000000000, 0.928000000000000],
            ),
            Atom::new(
                "H",
                [0.404000000000000, 0.250000000000000, 0.280000000000000],
            ),
            Atom::new(
                "Li",
                [0.156800000000000, 0.250000000000000, 0.101500000000000],
            ),
            Atom::new(
                "H",
                [0.828000000000000, 0.554000000000000, 0.572000000000000],
            ),
            Atom::new(
                "H",
                [0.828000000000000, 0.946000000000000, 0.572000000000000],
            ),
            Atom::new(
                "H",
                [0.672000000000000, 0.054000000000000, 0.072000000000000],
            ),
            Atom::new(
                "B",
                [0.304000000000000, 0.250000000000000, 0.430500000000000],
            ),
            Atom::new(
                "B",
                [0.196000000000000, 0.750000000000000, 0.930500000000000],
            ),
            Atom::new(
                "H",
                [0.600000000000000, 0.750000000000000, 0.456000000000000],
            ),
            Atom::new(
                "B",
                [0.804000000000000, 0.250000000000000, 0.069500000000000],
            ),
            Atom::new(
                "Li",
                [0.343200000000000, 0.750000000000000, 0.601500000000000],
            ),
            Atom::new(
                "B",
                [0.696000000000000, 0.750000000000000, 0.569500000000000],
            ),
            Atom::new(
                "Li",
                [0.656800000000000, 0.250000000000000, 0.398500000000000],
            ),
            Atom::new(
                "H",
                [0.172000000000000, 0.446000000000000, 0.428000000000000],
            ),
            Atom::new(
                "H",
                [0.672000000000000, 0.446000000000000, 0.072000000000000],
            ),
        ];

        let template_poscar =
            Poscar::from_reader(test_poscars::POSCAR_BOROHYDRIDE.as_bytes()).unwrap();
        let template_poscar = template_poscar.into_raw();

        let poscar = atom_list_to_poscar(&mixed_atom_list, &template_poscar).unwrap();

        // Closure to find the symbol of an atom by its index in the POSCAR
        let get_atom_symbol = |i: usize| {
            let group_counts = poscar.group_counts();
            let group_symbols = poscar.group_symbols().unwrap();
            let group_counts_with_symbols = group_counts.zip(group_symbols);

            // Make atom indices start with 1 instead of 0
            let mut index = i + 1;

            for (count, symbol) in group_counts_with_symbols {
                if count >= index {
                    return symbol;
                } else {
                    index -= count;
                }
            }

            panic!(
                "Atom with index {} is out of range of group counts \
                   (total of {} sites).",
                i,
                poscar.num_sites()
            );
        };

        // Closure to retrieve `Atom` entry for provided atom position from the `AtomList`.
        let get_atom_entry = |position: &[f64; 3]| {
            for (i, entry) in mixed_atom_list.iter().enumerate() {
                if entry.position == *position {
                    return (i, entry);
                }
            }

            panic!(
                "Atom with position {:?} not found in the source atom list.",
                position
            );
        };

        // Checktable to mark each atom in atom list as found in POSCAR
        let mut atom_list_checktable = vec![false; mixed_atom_list.len()];

        // Check properties of all sites by iterating through their positions
        for (i, position) in poscar.frac_positions().iter().enumerate() {
            // Get the corresponding entry from the source `AtomList`
            let (atom_i, atom) = get_atom_entry(position);
            // Get atom symbol using group counts and symbol names from POSCAR
            let symbol = get_atom_symbol(i);

            // Compare the symbols
            assert_eq!(atom.symbol, symbol);
            atom_list_checktable[atom_i] = true;
        }

        // Check if all atoms in the atom list were found in POSCAR
        for (i, check) in atom_list_checktable.iter().enumerate() {
            if *check == false {
                let atom = &mixed_atom_list[i];
                panic!(
                    "Atom with symbol \"{}\" and position {:?} was not found in \
                       assembled POSCAR.",
                    atom.symbol, atom.position
                );
            }
        }

        // Check that lattice data was copied
        let raw_poscar = poscar.into_raw();
        assert_eq!(raw_poscar.scale, template_poscar.scale);
        assert_eq!(raw_poscar.lattice_vectors, template_poscar.lattice_vectors);
    }
}
