mod atom_list;
mod decouple;
mod poscar_utils;

#[cfg(test)]
mod test_poscars;

use clap::{crate_authors, crate_description, crate_name, crate_version, App, Arg, ArgMatches};
use std::fs::File;
use std::io::Write;
use std::path::{Path, PathBuf};
use vasp_poscar::Poscar;

/// Program entry point.
fn main() {
    // Parse command line arguments
    let matches = parse_arguments();

    // Extract arguments
    let input_path = matches.value_of("input_file").unwrap();
    let filter_path = matches.value_of("filter_file").unwrap();
    let tolerance = matches.value_of("tolerance");
    let output_filtered_poscar = matches.value_of("filtered_output_name").unwrap();
    let output_leftover_poscar = matches.value_of("leftover_output_name").unwrap();
    let verbose = matches.is_present("verbose");
    let sort = matches.is_present("sort");

    let output_dir = match matches.value_of("output_directory") {
        Some(dir) => {
            let mut path = PathBuf::new();
            path.push(dir);
            // Check if the path points to a directory
            let metadata = match std::fs::metadata(&path) {
                Ok(meta) => meta,
                Err(e) => {
                    eprintln!("Could not get output directory metadata: {}", e);
                    std::process::exit(-1);
                }
            };
            if !metadata.is_dir() {
                eprintln!(
                    "Specified output directory path \"{}\" is not a directory.",
                    path.display()
                );
                std::process::exit(-1);
            }
            path
        }
        None => match std::env::current_dir() {
            Ok(path) => path,
            Err(e) => {
                eprintln!("Could not get current working directory: {}", e);
                std::process::exit(-1);
            }
        },
    };

    // Read input files
    let input_poscar = read_poscar_file("input POSCAR file", input_path);
    if verbose {
        println!("Read input file \"{}\".", input_path);
    }

    let filter_poscar = read_poscar_file("filter POSCAR file", filter_path);
    if verbose {
        println!("Read filter input file \"{}\".", input_path);
    }

    // Perform decoupling
    let decouple_result = match tolerance {
        // Tolerance provided - perform a tolerant decouple
        Some(tolerance) => match tolerance.parse::<f64>() {
            Ok(tolerance) => {
                if verbose {
                    println!(
                        "Performing tolerant decouple with tolerance of {} Å.",
                        tolerance
                    );
                }
                decouple::decouple_poscar_tolerant(&input_poscar, &filter_poscar, tolerance, sort)
            }
            Err(e) => {
                eprintln!("Unable to parse tolerance \"{}\": {}", tolerance, e);
                std::process::exit(-1);
            }
        },
        // Tolerance omitted - perform a simple decouple
        None => {
            if verbose {
                println!("Performing rigid decouple.");
            }
            decouple::decouple_poscar(&input_poscar, &filter_poscar, sort)
        }
    };

    // Handle decoupling results
    match decouple_result {
        // Decoupling successfull, write resulting files
        Ok((filtered_poscar, leftover_poscar)) => {
            write_poscar_file(
                "filtered POSCAR file",
                &filtered_poscar,
                &output_dir,
                output_filtered_poscar,
                verbose,
            );
            write_poscar_file(
                "leftover POSCAR file",
                &leftover_poscar,
                &output_dir,
                output_leftover_poscar,
                verbose,
            );
        }
        // Error while decoupling, only write error message
        Err(e) => {
            eprintln!("Error while decoupling POSCAR files: {}", e);
            std::process::exit(-1);
        }
    }
}

/// Parses command line arguments and returns the resulting matches.
fn parse_arguments<'a>() -> ArgMatches<'a> {
    return App::new(crate_name!())
        .version(crate_version!())
        .author(crate_authors!())
        .about(crate_description!())
        .arg(
            Arg::with_name("input_file")
                .short("i")
                .long("input")
                .value_name("INPUT_FILE")
                .help("Path to input POSCAR file")
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::with_name("filter_file")
                .short("f")
                .long("filter")
                .value_name("FILTER_FILE")
                .help("Path to filter POSCAR file")
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::with_name("tolerance")
                .short("t")
                .long("tolerance")
                .value_name("TOLERANCE")
                .help(
                    "Maximum distance (in units VASP uses for Cartesian \
                    coordinates) between matched atoms to be treated as \
                    equals (no tolerance by default)",
                )
                .takes_value(true),
        )
        .arg(
            Arg::with_name("output_directory")
                .short("o")
                .long("output-dir")
                .value_name("OUTPUT_DIRECTORY")
                .help(
                    "Path to the output directory to write decoupled POSCAR \
                    files into (into current working directory by default)",
                )
                .takes_value(true),
        )
        .arg(
            Arg::with_name("filtered_output_name")
                .long("filtered-output")
                .value_name("FILTERED_OUTPUT_NAME")
                .help("Name of the filtered output POSCAR file")
                .takes_value(true)
                .default_value("POSCAR-filtered"),
        )
        .arg(
            Arg::with_name("leftover_output_name")
                .long("leftover-output")
                .value_name("LEFTOVER_OUTPUT_NAME")
                .help("Name of the leftover output POSCAR file")
                .takes_value(true)
                .default_value("POSCAR-leftover"),
        )
        .arg(
            Arg::with_name("verbose")
                .short("v")
                .long("verbose")
                .help("Print additional (non-error) information to the standard output"),
        )
        .arg(Arg::with_name("sort").short("s").long("sort").help(
            "Sort resulting atom lists to always obtain the same \
            order of atom groups in POSCAR",
        ))
        .get_matches();
}

/// Reads a POSCAR file with path `path` and returns it as a parsed [Poscar].
/// In case of errors it prints to `STDERR` and terminates the program.
fn read_poscar_file(title: &str, path: &str) -> Poscar {
    match Poscar::from_path(path) {
        Ok(poscar) => return poscar,
        Err(e) => {
            eprintln!("Failed to read {} \"{}\": {}", title, path, e);
            std::process::exit(-1);
        }
    }
}

/// Writes a POSCAR file `poscar` to a file with name `file_name` in directory
/// `output_dir`. `title` is used for error messages in case of errors.
///
/// If errors occur, error messages are written to STDERR and the program is
/// terminated.
///
/// If `verbose` is [true], non-error messages will be printed to STDOUT.
fn write_poscar_file<P: AsRef<Path>>(
    title: &str,
    poscar: &Poscar,
    output_dir: P,
    file_name: &str,
    verbose: bool,
) {
    // Generate path to the output file
    let mut path = PathBuf::new();
    path.push(output_dir);
    path.push(file_name);

    // Open the file and write POSCAR to it
    match File::create(&path) {
        Ok(mut file) => match write!(file, "{}", poscar) {
            Ok(_) => {
                if verbose {
                    println!("Wrote {} to \"{}\".", title, path.display());
                }
            }
            Err(e) => {
                eprintln!("Failed to write to {} \"{}\": {}", title, path.display(), e);
                std::process::exit(-1);
            }
        },
        Err(e) => {
            eprintln!("Failed to create {} \"{}\": {}", title, path.display(), e);
            std::process::exit(-1);
        }
    }
}
